import os
from pathlib import Path
from typing import List


def find_available_projects() -> List[Path]:
    discovered_projects: List[Path] = []

    root_dirpath = Path("projects")
    for node in os.listdir(str(root_dirpath)):
        dirpath = root_dirpath.joinpath(node)
        if dirpath.joinpath("SConscript").exists():
            discovered_projects.append(dirpath)

    return discovered_projects


def print_available_projects():
    available_projects = find_available_projects()
    if available_projects:
        print(f"=== Available Projects ===")
        print(" {}".format("\n".join([path.name for path in available_projects])))
    else:
        print("No projects found in './projects/'")
