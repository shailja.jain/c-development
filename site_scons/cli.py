"""
SCons command line interface
"""

import scons_utils
from SCons.Script import *


def init():
    AddOption(
        "--project",
        default="hello-world",
        choices=[project.name for project in scons_utils.find_available_projects()],
    )
    AddOption("--list-projects", action="store_true")
    AddOption("--verbose", action="store_true")
